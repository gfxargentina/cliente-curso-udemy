import React, { Component } from 'react';

class paginador extends Component {
   
    state = { 
        paginador : { 
                    //math.ceil redondea para arriba, math.floor redondea para abajo 
            paginas : Math.ceil(Number(this.props.totalClientes) / this.props.limite )
        }
     }

    render() { 

        const {actual} = this.props;
        //boton anterior aparace si hay mas de 1 pagina
        const btnAnterior = (actual > 1) ? <button onClick={this.props.paginaAnterior} type="button" className="btn btn-success mr-2" >&laquo; Anterior</button> : '';
        //boton siguiente
        const {paginas} = this.state.paginador;
        const btnSiguiente = (actual !== paginas ) ? <button onClick={this.props.paginaSiguiente} type="button" className="btn btn-success ">Siguiente &raquo;</button> : '';
        
        
        return (  
            <div className="mt-5 d-flex justify-content-center">
                {btnAnterior}
                {btnSiguiente}
            </div>
        );
    }
}
 
export default paginador;