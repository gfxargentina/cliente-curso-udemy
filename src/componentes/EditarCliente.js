import React, { Component, Fragment } from 'react';
import { CLIENTE_QUERY } from '../queries';
import { Query } from 'react-apollo';
import FormularioEditarcliente from './FormularioEditarCliente';



class EditarCliente extends Component {
    state = {  }
    render() { 
        //tomar el id del contacto a editar
        const { id } = this.props.match.params;
        console.log(id);

        return ( 
            <Fragment>
            <h2 className="text-center">Editar Cliente</h2>
            <div className="row justify-content-center">
            <Query query={CLIENTE_QUERY} variables={{id}} >
                {({loading, error, data, refetch}) =>{
                    if(loading) return "cargando...";
                    if(error) return `Error! ${error.message}`;
                    console.log(data);
                    return (
                        <FormularioEditarcliente 
                                cliente={data.getCliente}
                                refetch={refetch}
                        />
                            
                        
                    )
                }

                }

            </Query>

            </div>
            </Fragment>

         );
    }
}
 
export default EditarCliente;